package cn.edu.zucc.wuh.dao;

import cn.edu.zucc.wuh.entity.Project;
import cn.edu.zucc.wuh.form.MarkedForm;

import java.util.List;

public interface ProjectMapper {
    int deleteByPrimaryKey(String pid);

    int insert(Project record);

    int insertSelective(Project record);

    Project selectByPrimaryKey(String pid);

    int updateByPrimaryKeySelective(Project record);

    int updateByPrimaryKey(Project record);

    List<Project> getProjectAll();

    List<Project> getUnmarkedProject(String host);
    List<MarkedForm> getMarkedProject(String host);
}