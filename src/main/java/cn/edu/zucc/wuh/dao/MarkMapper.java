package cn.edu.zucc.wuh.dao;

import cn.edu.zucc.wuh.entity.Mark;

import java.util.List;

public interface MarkMapper {
    int deleteByPrimaryKey(String mid);

    int insert(Mark record);

    int insertSelective(Mark record);

    Mark selectByPrimaryKey(String mid);

    int updateByPrimaryKeySelective(Mark record);

    int updateByPrimaryKey(Mark record);

    List<Mark> getMarkByUser(String host);

    List<Mark> getMarkAll();

    Mark selectByPidAndHost(String pid,String host);
}