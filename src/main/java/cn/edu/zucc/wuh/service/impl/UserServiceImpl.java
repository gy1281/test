package cn.edu.zucc.wuh.service.impl;

import cn.edu.zucc.wuh.dao.UserMapper;
import cn.edu.zucc.wuh.entity.User;
import cn.edu.zucc.wuh.form.UserEditForm;
import cn.edu.zucc.wuh.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * (User)表服务实现类
 *
 * @author makejava
 * @since 2020-06-18 12:18:18
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public Boolean isUserExist(String host) {
        User user=userMapper.selectByPrimaryKey(host);
        if (user != null){
            return true;
        }
        return false;
    }

    @Override
    public User selectByPrimaryKey(String host) {
        return userMapper.selectByPrimaryKey(host);
    }

    @Override
    public List<User> getUserAll() {
        return userMapper.getUserAll();
    }

    @Override
    public User editUser(UserEditForm form) {
        User user = userMapper.selectByPrimaryKey(form.getHost());
        user.setHost(form.getHost());
        user.setName(form.getName());
        user.setCourse(form.getCourse());
        user.setDegree(form.getDegree());
        user.setPhone(form.getPhone());
        user.setEmail(form.getEmail());
        user.setSchool(form.getSchool());
        user.setTitle(form.getTitle());
        user.setYearOfTeaching(form.getYearOfTeaching());
        userMapper.updateByPrimaryKey(user);
        return user;
    }

    @Override
    public List<User> queryUserByHostAndPassword(String host, String pwd) {
        return  userMapper.queryUserByHostAndPassword(host,pwd);
    }

    @Override
    public User editPwd(String host, String pwd) {
        User user = userMapper.selectByPrimaryKey(host);
        String md5Str = DigestUtils.md5DigestAsHex(pwd.getBytes());
        user.setPassword(md5Str);
        userMapper.updateByPrimaryKey(user);
        return user;
    }

    @Override
    public User login(String host, String pwd) {
        String md5Str = DigestUtils.md5DigestAsHex(pwd.getBytes());
        return userMapper.queryUserByHostAndPassword(host,md5Str).get(0);
    }

    /**
     * 通过ID查询单条数据
     *
     * @param host 主键
     * @return 实例对象
     */
    @Override
    public User queryById(String host) {
        return this.userMapper.selectByPrimaryKey(host);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<User> queryAllByLimit(int offset, int limit) {
        return this.userMapper.getUserAll();
    }

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public User insert(User user) {
        String md5Str= DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
        user.setPassword(md5Str);
        this.userMapper.insert(user);
        return user;
    }

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public User update(User user) {
        this.userMapper.updateByPrimaryKey(user);
        return this.selectByPrimaryKey(user.getHost());
    }

    /**
     * 通过主键删除数据
     *
     * @param host 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String host) {
        return this.userMapper.deleteByPrimaryKey(host) > 0;
    }
}