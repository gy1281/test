package cn.edu.zucc.wuh.service.impl;

import cn.edu.zucc.wuh.dao.ProjectMapper;
import cn.edu.zucc.wuh.entity.Project;
import cn.edu.zucc.wuh.form.MarkedForm;
import cn.edu.zucc.wuh.service.ProjectService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Project)表服务实现类
 *
 * @author makejava
 * @since 2020-06-18 12:18:05
 */
@Service("projectService")
public class ProjectServiceImpl implements ProjectService {
    @Resource
    private ProjectMapper projectMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param pid 主键
     * @return 实例对象
     */
    @Override
    public Project queryById(String pid) {
        return this.projectMapper.selectByPrimaryKey(pid);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Project> queryAllByLimit(int offset, int limit) {
        return this.projectMapper.getProjectAll();
    }

    /**
     * 新增数据
     *
     * @param project 实例对象
     * @return 实例对象
     */
    @Override
    public Project insert(Project project) {
        this.projectMapper.insert(project);
        return project;
    }

    /**
     * 修改数据
     *
     * @param project 实例对象
     * @return 实例对象
     */
    @Override
    public Project update(Project project) {
        this.projectMapper.updateByPrimaryKey(project);
        return this.queryById(project.getPid());
    }

    /**
     * 通过主键删除数据
     *
     * @param project 实例对象
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Project project) {
        return this.projectMapper.updateByPrimaryKey(project) > 0;
    }

    @Override
    public List<Project> getProjectAll() {
        return projectMapper.getProjectAll();
    }

    @Override
    public List<Project> getUnmarkedProject(String host) {
        return projectMapper.getUnmarkedProject(host);
    }

    @Override
    public List<MarkedForm> getMarkedProject(String host) {
        return projectMapper.getMarkedProject(host);
    }
}