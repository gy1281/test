package cn.edu.zucc.wuh.controller;

import cn.edu.zucc.wuh.entity.Mark;
import cn.edu.zucc.wuh.entity.User;
import cn.edu.zucc.wuh.service.MarkService;
import cn.edu.zucc.wuh.util.JsonUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * (Mark)表控制层
 *
 * @author makejava
 * @since 2020-06-18 12:18:12
 */
//@CrossOrigin(origins = "http://39.99.211.127", maxAge = 3600)
@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
@RestController
@RequestMapping("mark")
public class MarkController {
    private JsonUtil util = new JsonUtil();
    /**
     * 服务对象
     */
    @Resource
    private MarkService markService;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Mark selectOne(String id) {
        return this.markService.queryById(id);
    }
    /**
     * 打分
     */
    @PostMapping("/submit_mark/{host}")
    public JSONObject submit(@PathVariable("host") String host, int score,String pid){
        JSONObject ret = new JSONObject();

        Mark mark=new Mark();
        List<Mark> marks=markService.getMarkAll();
        Mark markold=markService.getMarkByPidAndHost(pid,host);


        int i=marks.size()+1;
        String mid=String.valueOf(i);
        mark.setMid(mid);
        mark.setPid(pid);
        mark.setScore(score);
        mark.setCreateUser(host);
        Date time=new Date();

        mark.setLastModifiedTime(time);
        mark.setStatus(0);
        if(markold==null) {
            mark.setCreateTime(time);
            Mark marknew = markService.insert(mark);
            ret=util.Mark2Json(marknew);
            return ret;
        }
        mark.setMid(markold.getMid());
        Mark marknew=markService.update(mark);
        ret=util.Mark2Json(marknew);
        return ret;


    }
    /**
     * 修改打分
     */
    @PostMapping("/modify_mark/{host}")
    public JSONObject modify(@PathVariable("host") String host, int score,String pid){
        JSONObject ret = new JSONObject();

        Mark mark=markService.getMarkByPidAndHost(pid,host);
        mark.setScore(score);
        Date time=new Date();
        mark.setLastModifiedTime(time);

        Mark marknew=markService.update(mark);
        ret=util.Mark2Json(marknew);
        return ret;
    }
    /**
     * 查询打分
     */
    @GetMapping("/get_mark_info/{host}")
    public JSONObject info(@PathVariable("host") String host, String pid){
        JSONObject ret = new JSONObject();

        Mark mark=markService.getMarkByPidAndHost(pid,host);

        ret=util.Mark2Json(mark);
        return ret;
    }
}