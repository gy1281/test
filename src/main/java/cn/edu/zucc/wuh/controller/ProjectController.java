package cn.edu.zucc.wuh.controller;

import cn.edu.zucc.wuh.entity.Mark;
import cn.edu.zucc.wuh.entity.Project;
import cn.edu.zucc.wuh.form.MarkedForm;
import cn.edu.zucc.wuh.service.MarkService;
import cn.edu.zucc.wuh.service.ProjectService;
import cn.edu.zucc.wuh.util.JsonUtil;
import cn.edu.zucc.wuh.util.PageUtil;
import cn.edu.zucc.wuh.util.SpringUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Project)表控制层
 *
 * @author makejava
 * @since 2020-06-18 12:18:06
 */
//@CrossOrigin(origins = "http://39.99.211.127", maxAge = 3600)
@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
@RestController
@RequestMapping("project")
public class ProjectController {
    /**
     * 服务对象
     */
    @Autowired
    private ProjectService projectService;

    @Autowired
    private MarkService markService;
    private JsonUtil util = new JsonUtil();
    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Project selectOne(String id) {
        return this.projectService.queryById(id);
    }
    /**
     * 查看所有项目
     */
    @GetMapping("/projects")
    public JSONObject listProjects(@RequestParam("page_num") int pageNum,
                                          @RequestParam("page_size") int pageSize) {
        System.out.println(11111);
        List<Project> projects = projectService.getProjectAll();
        System.out.println(projects);
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize);
        Page<Project> page = PageUtil.createPageFromList(projects, pageable);
        JSONObject ret = util.ProjectPage2Json(page);

        return ret;
    }
    /**
     * 查看项目详情
     */
    @GetMapping("/get_info/{pid}")
    public JSONObject getInfo(@PathVariable String pid){
        JSONObject ret = new JSONObject();

        Project project = projectService.queryById(pid);
        ret = util.Project2Json(project);

        return ret;
    }
    /**
     * 查看已打分项目详情
     */
    @GetMapping("/projects/{host}")
    public JSONObject listMarkedProjects(@PathVariable String host,@RequestParam("page_num") int pageNum,
                                          @RequestParam("page_size") int pageSize) {
        JSONObject ret = new JSONObject();

        List<MarkedForm> projects=null;
        projects=projectService.getMarkedProject(host);
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize);
        Page<MarkedForm> page = PageUtil.createPageFromList(projects, pageable);
        ret = util.MarkedProjectPage2Json(page);

        return ret;
    }
    /**
     * 查看未打分项目详情
     */
    @GetMapping("/projects/unmarked/{host}")
    public JSONObject listUnmarkedProjects(@PathVariable String host,@RequestParam("page_num") int pageNum,
                                         @RequestParam("page_size") int pageSize) {
        JSONObject ret = new JSONObject();
        List<Project> projects=null;
        projects=projectService.getUnmarkedProject(host);

        Pageable pageable = PageRequest.of(pageNum - 1, pageSize);
        Page<Project> page = PageUtil.createPageFromList(projects, pageable);
        ret = util.ProjectPage2Json(page);

        return ret;
    }
}