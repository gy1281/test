package cn.edu.zucc.wuh.controller;

import cn.edu.zucc.wuh.entity.User;
import cn.edu.zucc.wuh.form.UserEditForm;
import cn.edu.zucc.wuh.service.UserService;
import cn.edu.zucc.wuh.util.EmailUtil;
import cn.edu.zucc.wuh.util.JsonUtil;
import cn.edu.zucc.wuh.util.Statue;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.mail.EmailException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (User)表控制层
 *
 * @author makejava
 * @since 2020-06-18 12:18:19
 */
//@CrossOrigin(origins = "http://39.99.211.127", maxAge = 3600)
@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
@RestController
@RequestMapping("user")
@Api(tags = "用户管理")
public class UserController {
    private JsonUtil util = new JsonUtil();
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    /**
     *  注册
     */
    @PostMapping("/register")
    @ApiOperation("根据用户名密码注册")
    @ApiImplicitParam(name = "user", value = "用户注册表", required = true, dataType = "User", paramType = "body")
    public JSONObject register(@RequestBody User user) {
        JSONObject ret = new JSONObject();

        // host已经存在
        if (userService.isUserExist(user.getHost()) == true) {
            ret.put("code", "error");
            ret.put("msg", "该用户已经存在");
            return ret;
        }
        User usernew = userService.insert(user);
        ret = util.user2JSON(usernew);

        return ret;
    }
    /**
     * 登陆
     */
    @PostMapping("/login/{host}")
    public JSONObject login(@PathVariable("host") String host, String password){
        JSONObject ret = new JSONObject();

        if (userService.isUserExist(host) == false){
            ret.put("code", "error");
            ret.put("msg", "账号不存在");
            return ret;
        }

        User user = userService.login(host, password);
        if (user == null) {
            ret.put("code", "error");
            ret.put("msg", "密码错误");
            return ret;
        } else {
            ret = util.user2JSON(user);
        }

        return ret;
    }
    /**
     * 获取个人信息
     */
    @GetMapping("/{host}/get_personal_info")
    public JSONObject getPersonalInfo(@PathVariable("host") String host) {
        JSONObject ret = new JSONObject();
        User user = userService.selectByPrimaryKey(host);
        ret = util.userInfo2Json(user);
        return ret;
    }
    /**
     * 修改个人信息
     */
    @PostMapping("/edit_personal_info")
    public JSONObject editInfo(@RequestBody UserEditForm userEditForm) {
        JSONObject ret = new JSONObject();
        User user = userService.editUser(userEditForm);

        ret = util.userInfo2Json(user);
        return ret;
    }
    /**
     * 修改密码
     */
    @PostMapping("/{host}/edit_pwd")
    public JSONObject editPwd(@PathVariable("host") String host, String pwd1, String pwd2){
        JSONObject ret = new JSONObject();
        if (!pwd1.equals(pwd2)){
            ret.put("code", "error");
            ret.put("msg", "");
        }

        User user = userService.editPwd(host, pwd1);
        ret = util.userInfo2Json(user);

        return ret;
    }
    @GetMapping("getUserAll")
    public ResponseEntity<?> getUserAll(){
        return ResponseEntity.ok(userService.getUserAll());
    }
    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public User selectOne(String id) {
        return this.userService.queryById(id);
    }
    /**
     * 发送邮件，获取邮箱验证码
     */
    @PostMapping("/send_email")
    public JSONObject sendEmail(@RequestParam String address) throws EmailException {
        JSONObject ret = new JSONObject();

        EmailUtil emailUtil = new EmailUtil();
        String vc = emailUtil.sendEmail(address);
        ret.put("code", Statue.SUCCESS);
        JSONObject tmp = new JSONObject();
        tmp.put("vc", vc);
        ret.put("data", tmp);

        return ret;
    }

}